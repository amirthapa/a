//
//  HotelViewController.swift
//  A
//
//  Created by digital nepal on 5/10/19.
//  Copyright © 2019 Amir Tech. All rights reserved.
//

import UIKit
import WebKit

class HotelViewController: UIViewController,UIWebViewDelegate {
    @IBOutlet var webView_Hotel: UIWebView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        webView_Hotel.delegate = self
        title = "Hotel"
        self.showHud("Loading Result")
        loadFromAPI(offSetNormal: 0)
        
        // Do any additional setup after loading the view.
    }
    func webViewDidFinishLoad(_ webView: UIWebView) {
        
        self.webView_Hotel.scalesPageToFit = true
        self.webView_Hotel.contentMode = UIView.ContentMode.scaleAspectFit
        
        
        self.hideHUD()
    }
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        
    }
    
    func loadFromAPI(offSetNormal:Int){
        
        
        let url = URL(string:"https://hotel.galaxynp.com/app")
        
        let requestObj = URLRequest(url: (url)!)
        self.webView_Hotel.loadRequest(requestObj)
        
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

