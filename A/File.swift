//
//  File.swift
//  A
//
//  Created by digital nepal on 5/6/19.
//  Copyright © 2019 Amir Tech. All rights reserved.
//

import Foundation
import UIKit
import MBProgressHUD

extension UIViewController {
    func showHud(_ message: String) {
        let loadingNotification = MBProgressHUD.showAdded(to: (self.view)!, animated: true)
        loadingNotification.mode = MBProgressHUDMode.indeterminate
        loadingNotification.label.text = "Loading"
        loadingNotification.bezelView.color = UIColor.clear
        loadingNotification.contentColor = .blue
        loadingNotification.backgroundColor = UIColor.clear
        loadingNotification.isUserInteractionEnabled = false
    }
    func hideHUD() {
        MBProgressHUD.hide(for: self.view, animated: true)
    }
}

